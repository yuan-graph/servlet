package com.web.template.sax1;

import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.ext.DefaultHandler2;

public class Digester extends DefaultHandler2 {


  private StringBuilder bodyText = new StringBuilder();

  private ArrayStack<StringBuilder> pText = new ArrayStack<>();


  @Override
  public void characters(char[] ch, int start, int length) throws SAXException {
    bodyText.append(ch, start, length);
  }

  @Override
  public void startElement(String uri, String localName, String qName, Attributes attributes)
      throws SAXException {
    pText.push(bodyText);
    bodyText = new StringBuilder();
    System.out.println("标签 start：" + qName);
  }

  @Override
  public void endElement(String uri, String localName, String qName) throws SAXException {
    System.out.println(qName+" body:"+bodyText.toString());
    System.out.println("标签 end：" + qName);

    bodyText=pText.pop();
  }

}
