package com.web.template.sax;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;
import org.xml.sax.XMLReader;

public class SaxTest {

  public static void main(String[] args)
      throws ParserConfigurationException, SAXException, IOException {
    SAXParserFactory factory = SAXParserFactory.newInstance();
    SAXParser parser = factory.newSAXParser();
    XMLReader xmlReader = parser.getXMLReader();
    Digester digester=new Digester();
    xmlReader.setContentHandler(digester);
//    InputStream inputStream=new FileInputStream("xxx\\conf\\server.xml");
    InputStream inputStream=new FileInputStream("D:\\project\\servlet\\tomcat-start-demo\\src\\main\\java\\com\\web\\template\\sax\\test.xml");
    xmlReader.parse(new InputSource(inputStream));
  }

}
