package com.web.template;

public class Catalina {

  StandardServer server =new StandardServer();

  public StandardServer getServer() {
    return server;
  }

  public void load() {
    getServer().init();
  }

  public void start() {
    getServer().start();
  }

}
