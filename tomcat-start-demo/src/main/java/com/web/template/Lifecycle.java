package com.web.template;

public interface Lifecycle {

  public void init();

  public void start();

  public void stop();

  public void destroy();

}
