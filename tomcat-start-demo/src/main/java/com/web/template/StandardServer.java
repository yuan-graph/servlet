package com.web.template;

public final class StandardServer extends LifecycleBase {

  private StandardService[] services = new StandardService[1];

  {
    services[0] = new StandardService();
  }

  @Override
  protected void initInternal() {
    System.out.println("StandardServer init");
    for (StandardService service : services) {
      service.init();
    }
  }

  @Override
  protected void startInternal() {
    System.out.println("StandardServer start");
    for (StandardService service : services) {
      service.start();
    }
  }


}
