package com.web;

import javax.servlet.ServletConfig;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public class HelloServlet extends HttpServlet {

    @Override
    public void init(ServletConfig config) throws ServletException {
        System.out.println("HelloServlet servlet init..");
        ServletContext servletContext = config.getServletContext();
        String globalParam1 = servletContext.getInitParameter("globalParam1");
        System.out.println(globalParam1);

        servletContext.setAttribute("name", "zhangsan");

    }

    @Override
    public void destroy() {
        System.out.println("HelloServlet servlet destroy..");
    }

   /* @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp)
        throws ServletException, IOException {

        //获取编码
        String characterEncoding = req.getCharacterEncoding();
        System.out.println(characterEncoding);
        //获取请求URI
        String requestURI = req.getRequestURI();
        System.out.println(requestURI);
        //协议 版本
        String protocol = req.getProtocol();
        System.out.println(protocol);
        //主机域名
        String serverName = req.getServerName();
        System.out.println(serverName);
        //端口
        int serverPort = req.getServerPort();
        System.out.println(serverPort);
        //发布到tomcat下 项目名称
        String contextPath = req.getContextPath();
        System.out.println(contextPath);

        resp.getWriter().println("123");
    }*/

    /*protected void doGet(HttpServletRequest req, HttpServletResponse resp)
        throws ServletException, IOException {
        String name = req.getParameter("name");
        System.out.println(name);
        System.out.println("HelloServlet set name:" + name);
        req.setAttribute("name", name);
        req.getRequestDispatcher("/hello2").forward(req, resp);
    }*/

    protected void doGet(HttpServletRequest req, HttpServletResponse resp)
        throws ServletException, IOException {
        resp.sendRedirect("hello2");
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp)
        throws ServletException, IOException {
        doGet(req, resp);
    }
}
