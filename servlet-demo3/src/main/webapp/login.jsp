<%@ page contentType="text/html;charset=utf-8" %>
<html>
<head>
    <meta charset="text/html;charset=utf-8">
    <link type="text/css" href="bootstrap/bootstrap.min.css" rel="stylesheet"/>
    <script src="bootstrap/bootstrap.min.js"></script>
</head>
<body>
<div class="container">
    <div class="row">
        <div class="col-sm-6 col-sm-offset-3">
            <h1 class="text-center" style="margin-top:70px">用户登录</h1>
        </div>
    </div>
    <div class="row">
        <div class="col-sm-6 col-sm-offset-3">
            <form action="login" method="post">
                <div class="form-group">
                    <label>用户名：</label>
                    <input type="text" class="form-control" name="username" placeholder="用户名">
                </div>
                <div class="form-group">
                    <label>密码</label>
                    <input type="password" class="form-control" name="password" placeholder="请输入密码">
                </div>
                <div class="clearfix" style="color: red">${err}</div>
                <button type="submit" class="btn btn-danger col-sm-12" style="margin-top:10px;">登录</button>
            </form>
        </div>

    </div>

</div>

</body>
</html>