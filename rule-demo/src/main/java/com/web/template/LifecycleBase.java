package com.web.template;

public abstract class LifecycleBase implements Lifecycle {



  protected abstract void initInternal();

  protected abstract void startInternal();


  @Override
  public void init() {
    initInternal();
  }

  @Override
  public void start() {
    startInternal();
  }

  @Override
  public void stop() {

  }

  @Override
  public void destroy() {

  }
}
