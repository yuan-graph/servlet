package com.web.template.rule;

import java.io.FileInputStream;
import java.io.InputStream;
import org.apache.tomcat.util.digester.Digester;
import org.apache.tomcat.util.digester.ObjectCreateRule;
import org.apache.tomcat.util.digester.Rule;
import org.xml.sax.InputSource;

public class RuleTest {

  /**
   * 读取 server 根据规则创建StandardServer对象
   *
   * @param args
   * @throws Exception
   */
  public static void main(String[] args) throws Exception {
    //tomcat 的 Digester
    Digester digester = new Digester();
    //自建的
    digester.push(new Catalina());
    digester.push(new Catalina());
    //tomcat 创建类规则定义
    Rule rule = new ObjectCreateRule("com.web.template.StandardServer", "className");
    //添加规则，匹配name 为Server
    digester.addRule("Server", rule);
    //设置属性
    digester.addSetProperties("Server");
    //关联上下级关系，这里 把 StandardServer setServer 进 Catalina 里 规则定义
    digester.addSetNext("Server",
        "setServer",
        "com.web.template.StandardServer");
    InputStream inputStream = new FileInputStream("src/main/resources/server.xml");
    //sax解析 并监听，根据规则创建类
    digester.parse(new InputSource(inputStream));
  }

}
