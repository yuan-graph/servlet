package com.web.template;

public final class StandardServer extends LifecycleBase {

  private String shutdown = "SHUTDOWN";

  private int port = 8005;

  public StandardServer() {
    System.out.println("StandardServer 实例化完成。。");
  }

  private StandardService[] services = new StandardService[1];

  {
    services[0] = new StandardService();
  }

  @Override
  protected void initInternal() {
    System.out.println("StandardServer init");
    for (StandardService service : services) {
      service.init();
    }
  }

  @Override
  protected void startInternal() {
    System.out.println("StandardServer start");
    for (StandardService service : services) {
      service.start();
    }
  }


  public String getShutdown() {
    return shutdown;
  }

  public void setShutdown(String shutdown) {
    System.out.println("StandardServer shutdown设置属性：" + shutdown);
    this.shutdown = shutdown;
  }

  public int getPort() {
    return port;
  }

  public void setPort(int port) {
    System.out.println("StandardServer setPort设置属性：" + port);
    this.port = port;
  }
}
