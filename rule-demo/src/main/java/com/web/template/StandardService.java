package com.web.template;

public class StandardService extends LifecycleBase {

  @Override
  protected void initInternal() {
    System.out.println("StandardService init");
  }

  @Override
  protected void startInternal() {
    System.out.println("StandardService start");
  }
}
